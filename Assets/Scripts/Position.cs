﻿public class Position
{
    private int _column;
    private int _row;
    private bool _valued = false;

    public Position()
    {

    }

    public Position(int column, int row)
    {
        setPosition(column, row);
    }

    public void setPosition(int column, int row)
    {
        setRow(row);
        setColumn(column);
    }

    // Set the x value of the cell
    public void setColumn(int column)
    {
        _column = column;
    }

    // Set the y value of the cell
    public void setRow(int row)
    {
        _row = row;
    }

    // Get the x value of the cell
    public int getColumn()
    {
        return _column;
    }

    public void setValued()
    {
        _valued = true;
    }

    public bool isValued()
    {
        return _valued;
    }

    // Get the y value of the cell
    public int getRow()
    {
        return _row;
    }

    public static bool operator ==(Position pos1, object pos2)
    {

        if ((object)pos1 == null)
        {
            if ((object)pos2 == null)
            {
                // both are null and are Equal.
                return true;
            }

            // pos2 is not null, therefore not Equal.
            return false;
        }

        return (pos1.getColumn() == ((Position)pos2).getColumn() && 
                pos1.getRow() == ((Position)pos2).getRow());
    }
    public static bool operator !=(Position pos1, object pos2)
    {

        return !(pos1 == pos2);
    }



}
