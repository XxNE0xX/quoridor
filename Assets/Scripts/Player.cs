﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [SerializeField] private Sprite[] _pieceSprites;
    [SerializeField] private int yellowSpriteIndex = 4;
    [SerializeField] private int greenSpriteIndex = 5;

    [SerializeField] private GameManager gameManager;

    private Position _currentPosition = new Position();
    private Position _startingPosition = new Position();
    private int _playerIndex;
    private int _remainingWall;

    private bool _selected = false;
    private bool _isShadow = false;

    private void Start()
    {
        gameManager = transform.parent.GetComponent<GameManager>();
    }

    public void setCurrentPosition(Position pos)
    {
        _currentPosition.setPosition(pos.getColumn(), pos.getRow());
    }

    public void setCurrentPosition(int column, int row)
    {
        _currentPosition.setPosition(column, row);
    }

    public Position getCurrentPosition()
    {
        return _currentPosition;
    }

    public void setStartingPosition(int column, int row)
    {
        _startingPosition.setPosition(column, row);
    }

    public Position getStartingPosition()
    {
        return _startingPosition;
    }

    public void setPlayerIndex(int index)
    {
        _playerIndex = index;
    }

    public int getPlayerIndex()
    {
        return _playerIndex;
    }
    public void setRemainingWalls(int walls)
    {
        _remainingWall = walls;
    }

    public void ReduceRemainingWalls()
    {
        _remainingWall--;
    }

    public bool IsThereAnyWallRemaining()
    {
        return (_remainingWall > 0);
    }

    public bool isSelected()
    {
        return _selected;
    }

    public void setAsShadow()
    {
        _isShadow = true;
        setCurrentSprite();
    }

    public bool isShadow()
    {
        return _isShadow;
    }

    public void select()
    {
        _selected = true;
        setCurrentSprite();
    }

    public void deselect()
    {
        _selected = false;
        setCurrentSprite();
    }

    private void OnMouseUp()
    {
        if (gameManager.WhichPlayersTurn() == _playerIndex)
        {
            if (!_isShadow && !_selected)
            {
                select();
                gameManager.showPossiblePieceMoves(gameObject);
            }
            else if (_selected)
            {
                reset();
                gameManager.resetOngoingPLayerProcess();
            }
            else if (_isShadow)
            {
                gameManager.MovePieceTo(gameObject);
            }
        }
    }

    public void Move(Position destination)
    {
        setCurrentPosition(destination);
    }

    private void DestroyShadow()
    {
        if (_isShadow)
        {
            Destroy(gameObject);
        }
    }

    public void reset()
    {
        deselect();
        DestroyShadow();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            reset();
        }
    }

    public void setCurrentSprite()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = _pieceSprites[_playerIndex];
        if (_selected)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = _pieceSprites[yellowSpriteIndex];
        } else if (_isShadow)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = _pieceSprites[greenSpriteIndex];
        }
    }

}
