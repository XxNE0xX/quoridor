﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotManager : MonoBehaviour
{
    private Position position = new Position();

    int _row, _column;

    private bool _isMiddleOfAWall = false;
    private Direction _wallDirection = Direction.Null;
    private bool _availablity = false;
    private bool _selected = false;


    public void setPosition(int column, int row)
    {
        position.setPosition(column, row);
        _row = row;
        _column = column;
    }

    public void setMiddleOfAWall()
    {
        _isMiddleOfAWall = true;
    }

    public bool isMiddleOfAWall()
    {
        return _isMiddleOfAWall;
    }

    public void setWallDirection(Direction direction)
    {
        _wallDirection = direction;
    }

    public Direction getWallDirection()
    {
        return _wallDirection;
    }

    private void OnMouseEnter()
    {
        markColor();
    }

    private void OnMouseExit()
    {
        if (!_availablity && !_selected)
        {
            resetColor();
        }
        else if (_availablity)
        {
            availableColor();
        }
        else if (_selected)
        {
            markColor();
        }
    }

    private void OnMouseUp()
    {
        select();
        gameObject.GetComponentInParent<Board>().dotChoose(position);
    }

    private void resetColor()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void availableColor()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.green;
    }

    private void markColor()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;
    }

    public void select()
    {
        _selected = true;
        markColor();
    }

    public void deselect()
    {
        _selected = false;
        resetColor();
    }

    public void makeAvailable()
    {
        _availablity = true;
        availableColor();
    }

    public void disable()
    {
        _availablity = false;
        resetColor();
    }

    public void reset()
    {
        deselect();
        disable();
    }

    // TODO the right click handling should go to the board
    private void Update()
    {
        
    }

}
