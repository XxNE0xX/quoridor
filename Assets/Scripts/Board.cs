﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    private int _boardSize;
    private int _cellCount;
    private int _dotCount;

    private float _initialCellPose;
    private float _initialDotPose;
    private float _increment = 1.0f;

    // Had to give z for the sprite to detect the mouse
    [SerializeField] private float _dotZ = -1.0f;
    [SerializeField] private float _wallZ = -2.0f;
    [SerializeField] private float _playerZ = -3.0f;

    [SerializeField] private GameObject _cellPrefab;
    [SerializeField] private GameObject _dotPrefab;
    [SerializeField] private GameObject _wallPrefab;
    [SerializeField] private GameManager _gameManager;

    private GameObject[,] _cells;
    private GameObject[,] _dots;

    private ArrayList _walls = new ArrayList();


    private bool _isFirstChosenDot = true;
    private Position _firstChosenDotPosition = new Position();
    private ArrayList _availableDotOptionsPositions = new ArrayList();
    private ArrayList _availableCellOptionsPositions = new ArrayList();

    private void Awake()
    {
        InitializeVariables();
        instantiateCells();
        instantiateDots();
    }

    private void InitializeVariables()
    {
        _boardSize = _gameManager.getBoardSize();
        _cellCount = _gameManager.getCellCount();
        _dotCount = _gameManager.getDotCount();

        _initialCellPose = -((float)_cellCount / 2) + ((float)(_boardSize - _cellCount) / 2);
        _initialDotPose = -((float)_dotCount / 2) + ((float)(_dotCount - _cellCount) / 2);

        _cells = new GameObject[_cellCount, _cellCount];
        _dots = new GameObject[_dotCount, _dotCount];
    }

    public GameObject[,] getCells()
    {
        return _cells;
    }

    private void instantiateCells()
    {
        int counter = 0;
        for (int i = 0; i < _cellCount; i++)
        {
            for (int j = 0; j < _cellCount; j++)
            {
                _cells[j, i] = Instantiate(_cellPrefab) as GameObject;
                _cells[j, i].transform.position = new Vector2(_initialCellPose + _increment * j, _initialCellPose + _increment * i);
                // ATTENTION: the next two lines oreder matters
                _cells[j, i].GetComponent<CellManager>().initializeGraphNode(counter);
                _cells[j, i].GetComponent<CellManager>().getGraphNode().setPosition(j, i);
                _cells[j, i].transform.parent = gameObject.transform;
                counter++;
            }
        }
        _gameManager.setCellInstancingFinishedByBoardClass();
    }

    private void instantiateDots()
    {
        
        for (int i = 0; i < _dotCount; i++)
        {
            for (int j = 0; j < _dotCount; j++)
            {
                // We don't want to have dots on corners of the board
                if ((i == 0 && j == 0) || 
                    (i == 0 && j == _dotCount - 1) ||
                    (i == _dotCount - 1 && j == 0) ||
                    (i == _dotCount - 1 && j == _dotCount - 1))
                {
                    continue;
                }
                _dots[j, i] = Instantiate(_dotPrefab) as GameObject;
                _dots[j, i].transform.position = new Vector3(_initialDotPose + _increment * j, _initialDotPose + _increment * i, _dotZ);
                _dots[j, i].GetComponent<DotManager>().setPosition(j, i);
                _dots[j, i].transform.parent = gameObject.transform;
            }
        }
    }

    public void dotChoose(Position position)
    {
        int column = position.getColumn();
        int row = position.getRow();
        if (_isFirstChosenDot)
        {
            _firstChosenDotPosition.setPosition(column, row);
            _firstChosenDotPosition.setValued();
            _isFirstChosenDot = false;
            showAvailableOptions(_dots[position.getColumn(), position.getRow()]);
        }
        else
        {
            if (checkWallValidity(position))
            {
                drawWall(position);
                resetOngoingDotProgress();
                _dots[position.getColumn(), position.getRow()].GetComponent<DotManager>().reset();
            }
            else
            {
                Debug.Log("Invalid Wall");
            }
            _dots[position.getColumn(), position.getRow()].GetComponent<DotManager>().reset();
        }
    }

    private void showAvailableOptions(GameObject selectedDot)
    {
        DotManager selectedDotManager = selectedDot.GetComponent<DotManager>();
        if (selectedDotManager.getWallDirection() == Direction.Horizontal)
        {
            checkBotDotAvailablity(selectedDotManager.getWallDirection());
            checkTopDotAvailablity(selectedDotManager.getWallDirection());
        }
        else if (selectedDotManager.getWallDirection() == Direction.Vertical)
        {
            checkRightDotAvailablity(selectedDotManager.getWallDirection());
            checkLeftDotAvailablity(selectedDotManager.getWallDirection());
        }
        else
        {
            checkBotDotAvailablity(selectedDotManager.getWallDirection());
            checkTopDotAvailablity(selectedDotManager.getWallDirection());
            checkRightDotAvailablity(selectedDotManager.getWallDirection());
            checkLeftDotAvailablity(selectedDotManager.getWallDirection());
        }
        
    }

    // left dot check
    private void checkLeftDotAvailablity(Direction firstDotdirection)
    {
        // We can't place a vertical wall on first and last column
        if (_firstChosenDotPosition.getColumn() >= 2 &&
           (_firstChosenDotPosition.getRow() > 0 && _firstChosenDotPosition.getRow() < _dotCount - 1))
        {
            // check the middle dot, if it's already in the middle of another wall, we can't draw a wall there
            // also check if the destination is in the middle of another wall, unless the two walls are parallel or perpendicular we can't draw wall
            if (!_dots[_firstChosenDotPosition.getColumn() - 1, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().isMiddleOfAWall() &&
               ((!_dots[_firstChosenDotPosition.getColumn() - 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().isMiddleOfAWall() ||
                _dots[_firstChosenDotPosition.getColumn() - 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().getWallDirection() == firstDotdirection) ||
               (_dots[_firstChosenDotPosition.getColumn() - 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().getWallDirection() == Direction.Vertical &&
               firstDotdirection == Direction.Null)))
            {
                Position position = new Position();
                position.setPosition(_firstChosenDotPosition.getColumn() - 2, _firstChosenDotPosition.getRow());
                _availableDotOptionsPositions.Add(position);
                // light up the dot visually
                _dots[_firstChosenDotPosition.getColumn() - 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().makeAvailable();
            }
        }
    }

    // right dot check
    private void checkRightDotAvailablity(Direction firstDotdirection)
    {
        //if (Debug.isDebugBuild)
        //{
        //    Debug.Log("First: " + firstDotdirection);
        //    Debug.Log("Right: " + _dots[_firstChosenDotPosition.getColumn() + 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().isMiddleOfAWall() + _dots[_firstChosenDotPosition.getColumn() + 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().getWallDirection());
        //    Debug.Log("Cond3: " + (_dots[_firstChosenDotPosition.getColumn() + 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().getWallDirection() == firstDotdirection));
        //    Debug.Log("Cond1: " + (!_dots[_firstChosenDotPosition.getColumn() + 1, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().isMiddleOfAWall()));
        //}
        // We can't place a horizontal wall on first and last row
        if (_firstChosenDotPosition.getColumn() < _dotCount - 2 &&
           (_firstChosenDotPosition.getRow() > 0 && _firstChosenDotPosition.getRow() < _dotCount - 1))
        {
            // check the middle dot, if it's already in the middle of another wall, we can't draw a wall there
            // also check if the destination is in the middle of another wall, unless the two walls are parallel or perpendicular we can't draw wall
            if (!_dots[_firstChosenDotPosition.getColumn() + 1, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().isMiddleOfAWall() &&
               ((!_dots[_firstChosenDotPosition.getColumn() + 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().isMiddleOfAWall() ||
                _dots[_firstChosenDotPosition.getColumn() + 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().getWallDirection() == firstDotdirection) ||
               (_dots[_firstChosenDotPosition.getColumn() + 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().getWallDirection() == Direction.Vertical &&
               firstDotdirection == Direction.Null)))
            {
                Position position = new Position();
                position.setPosition(_firstChosenDotPosition.getColumn() + 2, _firstChosenDotPosition.getRow());
                _availableDotOptionsPositions.Add(position);
                // light up the dot visually
                _dots[_firstChosenDotPosition.getColumn() + 2, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().makeAvailable();
            }
        }
    }

    // top dot check
    private void checkTopDotAvailablity(Direction firstDotdirection)
    {
        //Debug.Log("First: " + firstDotdirection);
        //Debug.Log("Top: " + _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 2].GetComponent<DotManager>().isMiddleOfAWall() + _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 2].GetComponent<DotManager>().getWallDirection());
        // We can't place a vertical wall on first and last column
        if (_firstChosenDotPosition.getRow() < _dotCount - 2 && 
           (_firstChosenDotPosition.getColumn() > 0 && _firstChosenDotPosition.getColumn() < _dotCount - 1))
        {
            // check the middle dot, if it's already in the middle of another wall, we can't draw a wall there
            // also check if the destination is in the middle of another wall, unless the two walls are parallel or perpendicular we can't draw wall
            if (!_dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 1].GetComponent<DotManager>().isMiddleOfAWall() &&
               ((!_dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 2].GetComponent<DotManager>().isMiddleOfAWall() ||
               _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 2].GetComponent<DotManager>().getWallDirection() == firstDotdirection) ||
               (_dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 2].GetComponent<DotManager>().getWallDirection() == Direction.Horizontal &&
               firstDotdirection == Direction.Null)))
            {
                Position position = new Position();
                position.setPosition(_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 2);
                _availableDotOptionsPositions.Add(position);
                // light up the dot visually
                _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 2].GetComponent<DotManager>().makeAvailable();
            }
        }
    }

    // bottom dot check
    private void checkBotDotAvailablity(Direction firstDotdirection)
    {
        // We can't place a horizontal wall on first and last row
        if (_firstChosenDotPosition.getRow() >= 2 &&
           (_firstChosenDotPosition.getColumn() > 0 && _firstChosenDotPosition.getColumn() < _dotCount - 1))
        {
            // check the middle dot, if it's already in the middle of another wall, we can't draw a wall there
            // also check if the destination is in the middle of another wall, unless the two walls are parallel or perpendicular we can't draw wall
            if (!_dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 1].GetComponent<DotManager>().isMiddleOfAWall() &&
               ((!_dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 2].GetComponent<DotManager>().isMiddleOfAWall() ||
                _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 2].GetComponent<DotManager>().getWallDirection() == firstDotdirection) ||
               (_dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 2].GetComponent<DotManager>().getWallDirection() == Direction.Horizontal &&
               firstDotdirection == Direction.Null)))
            {
                Position position = new Position();
                position.setPosition(_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 2);
                _availableDotOptionsPositions.Add(position);
                // light up the dot visually
                _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 2].GetComponent<DotManager>().makeAvailable();
            }
        }
    }

    private bool checkWallValidity(Position position)
    {
        //Debug.Log("I'm in checkWallValidity: " + position.getRow() + " " + position.getColumn());
        foreach(Position pos in _availableDotOptionsPositions)
        {
            if (pos == position)
            {
                return true;
            }
        }
        return false;
    }

    private void drawWall(Position position)
    {


        GameObject theWall = Instantiate(_wallPrefab) as GameObject;
        theWall.transform.parent = gameObject.transform;

        theWall.GetComponent<Wall>().setStartPosition(_firstChosenDotPosition);
        theWall.GetComponent<Wall>().setEndPosition(position);

        // Marking the middle of the wall for further processes
        // Setting the wall direction
        // Rotating the prefab 90 degrees
        if (position.getColumn() == _firstChosenDotPosition.getColumn())
        {
            if (position.getRow() > _firstChosenDotPosition.getRow())
            {
                _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 1].GetComponent<DotManager>().setMiddleOfAWall();
                _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 1].GetComponent<DotManager>().setWallDirection(Direction.Vertical);
                theWall.GetComponent<Wall>().setDirection(Direction.Vertical);
                theWall.GetComponent<Wall>().setMidPosition(_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() + 1);
                // Rotate the wall for drawing
                theWall.transform.rotation *= Quaternion.Euler(0, 0, 90);
            }
            else
            {
                _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 1].GetComponent<DotManager>().setMiddleOfAWall();
                _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 1].GetComponent<DotManager>().setWallDirection(Direction.Vertical);
                theWall.GetComponent<Wall>().setDirection(Direction.Vertical);
                theWall.GetComponent<Wall>().setMidPosition(_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow() - 1);
                // Rotate the wall for drawing
                theWall.transform.rotation *= Quaternion.Euler(0, 0, 90);
            }
        }
        else if (position.getRow() == _firstChosenDotPosition.getRow())
        {
            if (position.getColumn() > _firstChosenDotPosition.getColumn())
            {
                _dots[_firstChosenDotPosition.getColumn() + 1, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().setMiddleOfAWall();
                _dots[_firstChosenDotPosition.getColumn() + 1, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().setWallDirection(Direction.Horizontal);
                theWall.GetComponent<Wall>().setDirection(Direction.Horizontal);
                theWall.GetComponent<Wall>().setMidPosition(_firstChosenDotPosition.getColumn() + 1, _firstChosenDotPosition.getRow());
            }
            else
            {
                _dots[_firstChosenDotPosition.getColumn() - 1, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().setMiddleOfAWall();
                _dots[_firstChosenDotPosition.getColumn() - 1, _firstChosenDotPosition.getRow()].GetComponent<DotManager>().setWallDirection(Direction.Horizontal);
                theWall.GetComponent<Wall>().setDirection(Direction.Horizontal);
                theWall.GetComponent<Wall>().setMidPosition(_firstChosenDotPosition.getColumn() - 1, _firstChosenDotPosition.getRow());
            }
        }
        Position midPos = theWall.GetComponent<Wall>().getMidPosition();
        theWall.transform.position = new Vector3(_initialDotPose + _increment * midPos.getColumn(), _initialDotPose + _increment * midPos.getRow(), _dotZ);
        if (_gameManager.canWallBePlaced(theWall.GetComponent<Wall>().getMidPosition(), theWall.GetComponent<Wall>().getDirection()))
        {
            _walls.Add(theWall);
        }
        else
        {
            Destroy(theWall);
            Debug.Log("Invalid Wall due to limiting a player");
        }
    }

    public void cellChoose(Position position)
    {

    }


    private void resetByRightClick()
    {
        resetOngoingDotProgress();
    }

    private void resetOngoingDotProgress()
    {
        if (_firstChosenDotPosition.isValued())
        {
            // Reset the _firstChosenDotPosition
            _dots[_firstChosenDotPosition.getColumn(), _firstChosenDotPosition.getRow()].GetComponent<DotManager>().deselect();
            _firstChosenDotPosition = new Position();
        }

        // Reset the boolean
        _isFirstChosenDot = true;

        // Reset the visuals of the available dots and their available boolean
        foreach (Position pos in _availableDotOptionsPositions)
        {
            _dots[pos.getColumn(), pos.getRow()].GetComponent<DotManager>().disable();
        }

        // Reset the list
        _availableDotOptionsPositions = new ArrayList();
    }

    private void resetOngoingCellProgress()
    {
        // Reset the visuals of the available dots and their available boolean
        foreach (Position pos in _availableCellOptionsPositions)
        {
            _cells[pos.getColumn(), pos.getRow()].GetComponent<CellManager>().reset();
        }

        // Reset the list
        _availableCellOptionsPositions = new ArrayList();
    }

    public void drawPlayer(GameObject player, float rotation)
    {
        int column = player.GetComponent<Player>().getCurrentPosition().getColumn();
        int row = player.GetComponent<Player>().getCurrentPosition().getRow();
        player.transform.position = new Vector3(_initialCellPose + _increment * column, _initialCellPose + _increment * row, _playerZ);
        // first we reset the transform!
        player.transform.rotation = Quaternion.identity;
        player.transform.rotation *= Quaternion.Euler(0, 0, rotation);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            resetByRightClick();
        }
    }

}
