﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private Camera _mainCamera;

    [SerializeField] private GameObject _boardGameObject;
    [SerializeField] private static int _boardSize = 10;
    [SerializeField] private static int _cellCount = 9;
    [SerializeField] private static int _dotCount = 10;

    Graph graphOfTheBoard = new Graph(_cellCount);

    [SerializeField] private int _allWalls = 20;
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private int _playerCount = 4;
    private GameObject[] _players;
    private GameObject[] shadowPlayers;

    [SerializeField] private bool _rotationSwitch = false;

    private bool _playersInitialized = false;
    private bool _cellInstancingFinishedByBoardClass = false;
    private bool _graphInitialized = false;
    private bool _goalsCalculated = false;

    private int _turn = 0;
    private float _rotationValue = 0;
    private float _cameraRotationSpeed = 10.0f;


    // Start is called before the first frame update
    void Start()
    {
        InitializingAndInstantiantingPlayers();
    }

    public void setCellInstancingFinishedByBoardClass()
    {
        _cellInstancingFinishedByBoardClass = true;
    }

    private void InitializingAndInstantiantingPlayers()
    {
        _players = new GameObject[_playerCount];

        for (int i = 0; i < _playerCount; i++)
        {
            _players[i] = Instantiate(_playerPrefab) as GameObject;
            _players[i].GetComponent<Player>().setPlayerIndex(i);
            _players[i].transform.parent = gameObject.transform;
            _players[i].GetComponent<Player>().setRemainingWalls(_allWalls / _playerCount);
            switch (i)
            {
                case 0:
                    _players[i].GetComponent<Player>().setStartingPosition((int)Mathf.Floor((float)_cellCount / 2), 0);
                    _players[i].GetComponent<Player>().setCurrentPosition((int)Mathf.Floor((float)_cellCount / 2), 0);
                    break;
                case 1:
                    _players[i].GetComponent<Player>().setStartingPosition((int)Mathf.Floor((float)_cellCount / 2), _cellCount - 1);
                    _players[i].GetComponent<Player>().setCurrentPosition((int)Mathf.Floor((float)_cellCount / 2), _cellCount - 1);
                    break;
                case 2:
                    _players[i].GetComponent<Player>().setStartingPosition(0, (int)Mathf.Floor((float)_cellCount / 2));
                    _players[i].GetComponent<Player>().setCurrentPosition(0, (int)Mathf.Floor((float)_cellCount / 2));
                    break;
                case 3:
                    _players[i].GetComponent<Player>().setStartingPosition(_cellCount - 1, (int)Mathf.Floor((float)_cellCount / 2));
                    _players[i].GetComponent<Player>().setCurrentPosition(_cellCount - 1, (int)Mathf.Floor((float)_cellCount / 2));
                    break;
                default:
                    _players[i].GetComponent<Player>().setStartingPosition((int)Mathf.Floor((float)_cellCount / 2), (int)Mathf.Floor((float)_cellCount / 2));
                    _players[i].GetComponent<Player>().setCurrentPosition((int)Mathf.Floor((float)_cellCount / 2), (int)Mathf.Floor((float)_cellCount / 2));
                    break;
            }
            _players[i].GetComponent<Player>().reset();
            _boardGameObject.GetComponent<Board>().drawPlayer(_players[i], _rotationValue);
        }
        _playersInitialized = true;
    }

    public int getBoardSize()
    {
        return _boardSize;
    }

    public int getCellCount()
    {
        return _cellCount;
    }

    public int getDotCount()
    {
        return _dotCount;
    }

    private void initializeGraph(GameObject[,] cells)
    {
        for (int i = 0; i < _cellCount; i++)
        {
            for (int j = 0; j < _cellCount; j++)
            {
                GraphNode gn = cells[j, i].GetComponent<CellManager>().getGraphNode();
                graphOfTheBoard.addNode(gn);
                if (j > 0)
                {
                    graphOfTheBoard.addEdge(gn, cells[j - 1, i].GetComponent<CellManager>().getGraphNode());
                }
                if (i > 0)
                {
                    graphOfTheBoard.addEdge(gn, cells[j, i - 1].GetComponent<CellManager>().getGraphNode());
                }
            }
        }
        for (int i = 0; i < _players.Length; i++)
        {
            int currentPositionIndex = GetCurrentPositionIndexOnGraph(_players[i].GetComponent<Player>());
            graphOfTheBoard.SetPlayerHere(currentPositionIndex, true);
        }
        _graphInitialized = true;
    }

    public bool canWallBePlaced(Position wallMiddlePointPosition, Direction wallDirection)
    {
        Player player = _players[WhichPlayersTurn()].GetComponent<Player>();
        if (player.IsThereAnyWallRemaining())
        {
            int wallMiddlePointRow = wallMiddlePointPosition.getRow();
            int wallMiddlePointColumn = wallMiddlePointPosition.getColumn();
            Position[] playersCurrentPosition = new Position[_playerCount];
            for (int i = 0; i < _playerCount; i++)
            {

                playersCurrentPosition[i] = _players[i].GetComponent<Player>().getCurrentPosition();
            }
            if (wallDirection == Direction.Horizontal)
            {
                // If the wall is horizontal and the middle point is (1,1)
                // the [(0,1),(0,0)] and [(1,1),(1,0)] will be disconnected
                graphOfTheBoard.removeEdge(wallMiddlePointColumn - 1, wallMiddlePointRow, wallMiddlePointColumn - 1, wallMiddlePointRow - 1);
                graphOfTheBoard.removeEdge(wallMiddlePointColumn, wallMiddlePointRow, wallMiddlePointColumn, wallMiddlePointRow - 1);
                // If we fail to reach to all the goals we should not let placing wall take place
                if (!graphOfTheBoard.areGoalsAchievable(playersCurrentPosition, _playerCount))
                {
                    Debug.Log("Horiz: Lock");
                    graphOfTheBoard.addEdge(wallMiddlePointColumn - 1, wallMiddlePointRow, wallMiddlePointColumn - 1, wallMiddlePointRow - 1);
                    graphOfTheBoard.addEdge(wallMiddlePointColumn, wallMiddlePointRow, wallMiddlePointColumn, wallMiddlePointRow - 1);
                    return false;
                }
            }
            else if (wallDirection == Direction.Vertical)
            {
                // If the wall is vertical and the middle point is (1,1)
                // the [(1,0),(1,1)] and [(0,0),(0,1)] will be disconnected
                graphOfTheBoard.removeEdge(wallMiddlePointColumn - 1, wallMiddlePointRow, wallMiddlePointColumn, wallMiddlePointRow);
                graphOfTheBoard.removeEdge(wallMiddlePointColumn - 1, wallMiddlePointRow - 1, wallMiddlePointColumn, wallMiddlePointRow - 1);
                // If we fail to reach to all the goals we should not let placing wall take place
                if (!graphOfTheBoard.areGoalsAchievable(playersCurrentPosition, _playerCount))
                {
                    Debug.Log("Vert: Lock");
                    graphOfTheBoard.addEdge(wallMiddlePointColumn - 1, wallMiddlePointRow, wallMiddlePointColumn, wallMiddlePointRow);
                    graphOfTheBoard.addEdge(wallMiddlePointColumn - 1, wallMiddlePointRow - 1, wallMiddlePointColumn, wallMiddlePointRow - 1);
                    return false;
                }
            }
            GoToNextTurn();
            player.ReduceRemainingWalls();
            return true;
        }
        else
        {
            Debug.Log("No wall remaining for you!");
            return false;
        }
    }

    public void showPossiblePieceMoves(GameObject originPlayerObject)
    {
        Player player = _players[WhichPlayersTurn()].GetComponent<Player>();
        int currentPositionIndex = GetCurrentPositionIndexOnGraph(player);
        List<GraphNode> firstAdjPossibleOptionsForMove = graphOfTheBoard.getNeighbors(currentPositionIndex);
        List<GraphNode> finalPossibleOptionsForMove = new List<GraphNode>();

        Dictionary<GraphNode, List<GraphNode>> jumpPossibilities = new Dictionary<GraphNode, List<GraphNode>>();

        // If a player is adjacent to your cell, we ignore that destination and get its adjacancies as new possible options
        for (int i = 0; i < firstAdjPossibleOptionsForMove.Count; i++)
        {
            if (firstAdjPossibleOptionsForMove[i].IsPlayerHere())
            {
                jumpPossibilities.Add(firstAdjPossibleOptionsForMove[i], new List<GraphNode>());
                // we get the adjacent vacant cells
                for (int j = 0; j < firstAdjPossibleOptionsForMove[i].getNeighbors().Count; j++)
                {
                    GraphNode graphNode = firstAdjPossibleOptionsForMove[i].getNeighbors()[j];
                    if (graphNode.IsPlayerHere() == false)
                    {
                        jumpPossibilities[firstAdjPossibleOptionsForMove[i]].Add(graphNode);
                    }
                }
            }
        }
        // If a player is able to straight jump, then we're sure that he can't do a diagonal jump at the same position
        List<GraphNode> straightJumps = new List<GraphNode>();
        List<GraphNode> removingNodes = new List<GraphNode>();
        for (int i = 0; i < jumpPossibilities.Count; i++)
        {
            for (int j = 0; j < jumpPossibilities.ElementAt(i).Value.Count; j++)
            {
                Position possiblePosition = jumpPossibilities.ElementAt(i).Value[j].getPosition();
                if (possiblePosition.getColumn() == player.getCurrentPosition().getColumn())
                {
                    Debug.Log("first if: " + possiblePosition.getColumn());
                    straightJumps.Add(jumpPossibilities.ElementAt(i).Value[j]);
                    removingNodes.Add(jumpPossibilities.ElementAt(i).Key);
                    break;
                }
                if (possiblePosition.getRow() == player.getCurrentPosition().getRow())
                {
                    Debug.Log("second if: " + possiblePosition.getRow());
                    straightJumps.Add(jumpPossibilities.ElementAt(i).Value[j]);
                    removingNodes.Add(jumpPossibilities.ElementAt(i).Key);
                    break;
                }
            }
        }
        for (int i = 0; i < removingNodes.Count; i++)
        {
            jumpPossibilities.Remove(removingNodes[i]);
        }
        finalPossibleOptionsForMove.AddRange(straightJumps);
        for (int i = 0; i < jumpPossibilities.Count; i++)
        {
            finalPossibleOptionsForMove.AddRange(jumpPossibilities.ElementAt(i).Value);
        }
        firstAdjPossibleOptionsForMove = firstAdjPossibleOptionsForMove.Where(t => t.IsPlayerHere() != true).ToList();
        finalPossibleOptionsForMove.AddRange(firstAdjPossibleOptionsForMove);


        shadowPlayers = new GameObject[finalPossibleOptionsForMove.Count];
        for (int i = 0; i < finalPossibleOptionsForMove.Count; i++)
        {
            int row = finalPossibleOptionsForMove[i].getIndex() / _cellCount;
            int column = finalPossibleOptionsForMove[i].getIndex() % _cellCount;
            shadowPlayers[i] = Instantiate(_playerPrefab) as GameObject;
            shadowPlayers[i].GetComponent<Player>().setAsShadow();
            shadowPlayers[i].GetComponent<Player>().setPlayerIndex(WhichPlayersTurn());
            shadowPlayers[i].transform.parent = gameObject.transform;
            shadowPlayers[i].GetComponent<Player>().setCurrentPosition(new Position(column, row));
            _boardGameObject.GetComponent<Board>().drawPlayer(shadowPlayers[i], _rotationValue);
            //_boardGameObject.GetComponent<Board>().markAvailableCell(row, column);
        }
    }

    private int GetCurrentPositionIndexOnGraph(Player player)
    {
        return player.getCurrentPosition().getRow() * _cellCount + player.getCurrentPosition().getColumn();
    }

    public void MovePieceTo(GameObject destinationPlayerObject)
    {
        Player player = _players[WhichPlayersTurn()].GetComponent<Player>();
        // reseting the isPlayerHere value in origin
        int originIndex = GetCurrentPositionIndexOnGraph(player);
        graphOfTheBoard.SetPlayerHere(originIndex, false);
        player.Move(destinationPlayerObject.GetComponent<Player>().getCurrentPosition());
        // setting the isPlayerHere value in destination
        int destinationIndex = GetCurrentPositionIndexOnGraph(player);
        graphOfTheBoard.SetPlayerHere(destinationIndex, true);
        player.reset();
        if (_rotationSwitch == false)
        {
            _boardGameObject.GetComponent<Board>().drawPlayer(player.gameObject, _rotationValue);
        }
        for (int i = 0; i < shadowPlayers.Length; i++)
        {
            Destroy(shadowPlayers[i]);
        }
        shadowPlayers = null;

        // check win
        if (graphOfTheBoard.CheckWin(WhichPlayersTurn(), _players[WhichPlayersTurn()].GetComponent<Player>().getCurrentPosition()))
        {
            Debug.Log("The player" + WhichPlayersTurn() + " has won!");
        }
        
        GoToNextTurn();
    }

    public void resetOngoingPLayerProcess()
    {
        for (int i = 0; i < shadowPlayers.Length; i++)
        {
            Destroy(shadowPlayers[i]);
        }
        shadowPlayers = null;
    }

    private void GoToNextTurn()
    {
        _turn++;
        if (_rotationSwitch == true)
        {
            RotatingObjects();
        }
    }

    private void RotatingObjects()
    {
        switch (WhichPlayersTurn())
        {
            case 0:
                _rotationValue = 0;
                break;
            case 1:
                _rotationValue = 180;
                break;
            case 2:
                _rotationValue = -90;
                break;
            case 3:
                _rotationValue = 90;
                break;
            default:
                _rotationValue = 0;
                break;
        }
        for (int i = 0; i < _players.Length; i++)
        {
            _boardGameObject.GetComponent<Board>().drawPlayer(_players[i], _rotationValue);
        }
        RotateCamera();
    }

    private void RotateCamera()
    {
        // first we reset the rotation
        _mainCamera.transform.rotation = Quaternion.identity;
        _mainCamera.transform.rotation *= Quaternion.Euler(0, 0, _rotationValue);
    }

    public int WhichPlayersTurn()
    {
        return (_turn % _playerCount);
    }

    public void SetNumberOfPlayers(int count)
    {
        _playerCount = count;
    }

    private void Update()
    {

        if (!_graphInitialized && _cellInstancingFinishedByBoardClass && _playersInitialized)
        {
            initializeGraph(_boardGameObject.GetComponent<Board>().getCells());
        }

        if (_graphInitialized && _playersInitialized && !_goalsCalculated)
        {
            Position[] startingPositions = new Position[_playerCount];

            for (int i = 0; i < _playerCount; i++)
            {
                startingPositions[i] = _players[i].GetComponent<Player>().getStartingPosition();
            }

            graphOfTheBoard.calculateGoals(_playerCount, startingPositions);
            _goalsCalculated = true;
        }
    }

}
