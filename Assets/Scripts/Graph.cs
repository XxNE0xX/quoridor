﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Graph
    {
        private List<GraphNode> _nodes = new List<GraphNode>();
        private GraphNode[,] _goals;
        private int _cellCount;

        public Graph(int cellCount)
        {
            _cellCount = cellCount;
        }

        public Graph(int nodesCount, int cellCount)
        {
            for (int i = 0; i < nodesCount; i++)
            {
                GraphNode graphNode = new GraphNode(i);
                _nodes.Add(graphNode);
            }
            _cellCount = cellCount;
        }

        public void calculateGoals(int numberOfPlayers, Position[] startingPositions)
        {
            _goals = new GraphNode[numberOfPlayers, _cellCount];
            for (int i = 0; i < numberOfPlayers; i++)
            {
                for (int j = 0; j < _cellCount; j++)
                {
                    if (startingPositions[i].getRow() == 0)
                    {
                        _goals[i, j] = _nodes[_cellCount * (_cellCount - 1) + j];
                       
                    }
                    if (startingPositions[i].getRow() == _cellCount - 1)
                    {
                        _goals[i, j] = _nodes[j];
                    }
                    if (startingPositions[i].getColumn() == 0)
                    {
                        _goals[i, j] = _nodes[_cellCount * j + (_cellCount - 1)];
                    }
                    if (startingPositions[i].getColumn() == _cellCount - 1)
                    {
                        _goals[i, j] = _nodes[j * _cellCount];
                    }
                }
            }
        }

        public bool CheckWin(int playerIndex, Position currentPosition)
        {
            for (int i = 0; i < _cellCount; i++)
            {
                if (_goals[playerIndex, i].getPosition() == currentPosition)
                {
                    return true;
                }
            }
            return false;
        }

        public bool areGoalsAchievable(Position[] playersCurrentPositions, int playerCount)
        {
            for (int i = 0; i < playerCount; i++)
            {
                if (!isGoalAchievable(playersCurrentPositions[i], i))
                {
                    if (Debug.isDebugBuild)
                    {
                        Debug.Log("Player " + i + " can't reach its goal");
                    }
                    return false;
                }
            }
            return true;
        }

        private bool isGoalAchievable(Position playerCurrentPosition, int playerIndex)
        {
            int playerCurrentPositionIndex = playerCurrentPosition.getRow() * _cellCount + playerCurrentPosition.getColumn();
            Debug.Log("Player " + playerIndex + " is at positionIndex: " + playerCurrentPositionIndex + "(" + playerCurrentPosition.getRow() + " " + playerCurrentPosition.getColumn() + ")");
            for (int i = 0; i < _cellCount; i++)
            {
                if (isReachable(playerCurrentPositionIndex, _goals[playerIndex, i].getIndex()))
                {
                    return true;
                }
            }
            return false;
        }

        // BFS-based search
        private bool isReachable (int startIndex, int endIndex)
        {
            if (startIndex == endIndex)
            {
                return true;
            }

            bool[] visited = new bool[_nodes.Count];
            for (int i = 0; i < _nodes.Count; i++)
            {
                visited[i] = false;
            }
            Queue<int> queue = new Queue<int>();

            visited[startIndex] = true;
            queue.Enqueue(startIndex);
            while (queue.Count != 0)
            {
                // Dequeue a vertex from queue
                startIndex = queue.Dequeue();

                List<GraphNode> neighbors = _nodes[startIndex].getNeighbors();

                foreach(GraphNode neighbor in neighbors)
                {
                    if (neighbor.getIndex() == endIndex)
                    {
                        return true;
                    }
                    if (!visited[neighbor.getIndex()])
                    {
                        visited[neighbor.getIndex()] = true;
                        queue.Enqueue(neighbor.getIndex());
                    }
                }
            }

            return false;
        }


        public void addNode(GraphNode graphNode)
        {
            _nodes.Add(graphNode);
        }

        public GraphNode getNodeAtIndex(int index)
        {
            return _nodes.ElementAt(index);
        }

        public void addEdge(int startIndex, int endIndex)
        {
            if (startIndex < 0 || startIndex > _nodes.Count - 1)
            {
                Debug.Log("at addEdge, Graph class: wrong start node index");
            }
            if (endIndex < 0 || endIndex > _nodes.Count - 1)
            {
                Debug.Log("at addEdge, Graph class: wrong end node index");
            }
            _nodes[startIndex].addNeighbor(_nodes[endIndex]);
            _nodes[endIndex].addNeighbor(_nodes[startIndex]);
        }

        public void addEdge(GraphNode startNode, GraphNode endNode)
        {
            if (!_nodes.Contains(startNode))
            {
                Debug.Log("at addEdge, Graph class: start node doesn't exist");
            }
            if (!_nodes.Contains(endNode))
            {
                Debug.Log("at addEdge, Graph class: end node doesn't exist");
            }
            startNode.addNeighbor(endNode);
            endNode.addNeighbor(startNode);
        }

        public void addEdge(int startColumn, int startRow, int endColumn, int endRow)
        {
            int startIndex = startRow * _cellCount + startColumn;
            int endIndex = endRow * _cellCount + endColumn;

            _nodes[startIndex].addNeighbor(_nodes[endIndex]);
            _nodes[endIndex].addNeighbor(_nodes[startIndex]);
        }

        public void removeEdge(int startIndex, int endIndex)
        {
            if (startIndex < 0 || startIndex > _nodes.Count - 1)
            {
                Debug.Log("at removeEdge, Graph class: wrong start node index");
            }
            if (endIndex < 0 || endIndex > _nodes.Count - 1)
            {
                Debug.Log("at removeEdge, Graph class: wrong end node index");
            }
            _nodes[startIndex].removeNeighbor(_nodes[endIndex]);
            _nodes[endIndex].removeNeighbor(_nodes[startIndex]);
        }

        public void removeEdge(int startColumn, int startRow, int endColumn, int endRow)
        {
            int startIndex = startRow * _cellCount + startColumn;
            int endIndex = endRow * _cellCount + endColumn;

            _nodes[startIndex].removeNeighbor(_nodes[endIndex]);
            _nodes[endIndex].removeNeighbor(_nodes[startIndex]);
        }

        public List<GraphNode> getNeighbors(int index)
        {
            return _nodes[index].getNeighbors();
        }

        public void SetPlayerHere(int graphNodeIndex, bool status)
        {
            _nodes[graphNodeIndex].SetPlayerHere(status);
        }

        public bool IsPlayerHere(int graphNodeIndex)
        {
            return _nodes[graphNodeIndex].IsPlayerHere();
        }

    }

}
