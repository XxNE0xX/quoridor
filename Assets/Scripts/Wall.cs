﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    private Position _startPosition = new Position();
    private Position _midPosition = new Position();
    private Position _endPosition = new Position();
    private Direction _direction = Direction.Null;

    public void setStartPosition(Position pos)
    {
        _startPosition.setPosition(pos.getColumn(), pos.getRow());
    }
    public void setMidPosition(Position pos)
    {
        _midPosition.setPosition(pos.getColumn(), pos.getRow());
    }

    public void setMidPosition(int row, int column)
    {
        _midPosition.setPosition(row, column);
    }

    public void setEndPosition(Position pos)
    {
        _endPosition.setPosition(pos.getColumn(), pos.getRow());
    }

    public void setDirection(Direction direction)
    {
        _direction = direction;
    }

    public Position getStartPosition()
    {
        return _startPosition;
    }
    public Position getMidPosition()
    {
        return _midPosition;
    }

    public Position getEndPosition()
    {
        return _endPosition;
    }

    public Direction getDirection()
    {
        return _direction;
    }
}
