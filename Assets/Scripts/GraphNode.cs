﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class GraphNode
    {
        private List<GraphNode> _neighbors = new List<GraphNode>();
        private int _index;
        private bool _playerHere = false;
        private Position position = new Position();

        public GraphNode(int index)
        {
            _index = index;
        }

        public int getIndex()
        {
            return _index;
        }

        public void SetPlayerHere(bool status)
        {
            _playerHere = status;
        }

        public bool IsPlayerHere()
        {
            return _playerHere;
        }

        public List<GraphNode> getNeighbors()
        {
            return _neighbors;
        }

        public void addNeighbor(GraphNode node)
        {
            if (!_neighbors.Contains(node))
            {
                _neighbors.Add(node);
            }
        }

        public void removeNeighbor(GraphNode node)
        {
            _neighbors.Remove(node);
        }

        public void setPosition(int column, int row)
        {
            position.setPosition(column, row);
        }

        public Position getPosition()
        {
            return position;
        }

        public static bool operator ==(GraphNode node1, GraphNode node2)
        {
            return (node1.getIndex() == node2.getIndex());
        }
        public static bool operator !=(GraphNode node1, GraphNode node2)
        {
            return (node1.getIndex() != node2.getIndex());
        }


    }
}
