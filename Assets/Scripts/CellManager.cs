﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellManager : MonoBehaviour
{
    
    private GraphNode graphNode;

    private bool _availablity = false;
    private bool _selected = false;

    public void initializeGraphNode(int index)
    {
        graphNode = new GraphNode(index);
    }

    public GraphNode getGraphNode()
    {
        return graphNode;
    }

    private void resetColor()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void availableColor()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.green;
    }

    private void markColor()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;
    }

    public void disable()
    {
        _availablity = false;
        resetColor();
    }

    public void select()
    {
        _selected = true;
        markColor();
    }

    public void deselect()
    {
        _selected = false;
        resetColor();
    }
    public void makeAvailable()
    {
        _availablity = true;
        availableColor();
    }

    public void reset()
    {
        deselect();
        disable();
    }

    private void OnMouseEnter()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color32(106, 0, 0, 255);
    }

    private void OnMouseExit()
    {
        if (_availablity)
        {
            availableColor();
        }
        else if (_selected)
        {
            markColor();
        }
        else
        {
            resetColor();
        }
    }

    private void OnMouseUp()
    {

    }

    private void Update()
    {

    }

}
